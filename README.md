#  Microsoft Graph Mail
> an implementation for the Microsoft Graph Mail API


## Install

```sh
$ npm install --save @gis-ag/microsoft-graph-mail
```

## Usage

After you require microsoft graph mail, you can easily setup the default properties.
```js
const MicrosoftGraphMail = require('@gis-ag/microsoft-graph-mail');

const defaults = {
  headers: {
    Authorization: 'Bearer AccessToken',
  },
};

const serviceOptions = {
  defaults,
  baseUrl: 'https://graph.microsoft.com/v1.0',
};

const service = new MicrosoftGraphMail(serviceOptions.baseUrl, serviceOptions);

```

In order to retrieve personal messages, simply invoke:

```js
const query = {
  id: 'Sent', // if not provided, Inbox is used by default,
  count: true,
};

const options = {}; // additional request options

service.messagesFromFolder(query, options, (err, mailConfig) => {
  // handle error and mailConfig object
})
```

`MailConfig` contains next data:
1. value -> list of requested messages
2. @odata.count -> number of items in the value array.

This is a list of all valid query parameters:
https://developer.microsoft.com/en-us/graph/docs/concepts/query_parameters

Note: Please do not provide $ along with query param, since that is handled by the service. Otherwise, query param will be ignored.

## License

MIT ©  [GIS AG](https://gis-ag.com)
