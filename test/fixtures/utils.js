'use strict';

// node core modules

// 3rd party modules
const _ = require('lodash');

// internal modules
const serviceFactory = require('../../lib');

const { unmocked, AUTH_TOKEN } = process.env;

const init = () => {
  const serviceOptions = { defaults: {} };
  if (unmocked) {
    Object.assign(serviceOptions.defaults, {
      headers: {
        Authorization: `Bearer ${AUTH_TOKEN}`,
      },
    });
  }
  const stringProps = ['id', 'subject', 'bodyPreview', 'parentFolderId', 'conversationId', 'webLink'];
  const dateProps = ['createdDateTime', 'lastModifiedDateTime', 'receivedDateTime', 'sentDateTime'];

  const validateEmailAddressFactory = t => (emailAddress) => {
    ['address', 'name'].forEach((prop) => {
      t.true(prop in emailAddress);
      t.true(_.isString(emailAddress[prop]));
    });
  };

  return {
    serviceOptions,
    stringProps,
    dateProps,
    serviceFactory,
    validateEmailAddressFactory,
  };
};

const initContext = t => _.assign(t.context, init());

module.exports = initContext;
