'use strict';

const nock = require('nock');

/* eslint-disable */

nock('https://graph.microsoft.com:443', { encodedQueryParams: true })
  .get('/v1.0/me/mailFolders/Wrong/messages')
  .reply(
    400,
    {
      error: {
        code: 'ErrorInvalidIdMalformed',
        message: 'Id is malformed.',
        innerError: { 'request-id': '95894327-2d49-40f1-a010-0e5528fe2cc8', date: '2018-01-10T16:51:17' },
      },
    },
    [
      'Cache-Control',
      'private',
      'Transfer-Encoding',
      'chunked',
      'Content-Type',
      'application/json',
      'request-id',
      '95894327-2d49-40f1-a010-0e5528fe2cc8',
      'client-request-id',
      '95894327-2d49-40f1-a010-0e5528fe2cc8',
      'x-ms-ags-diagnostic',
      '{"ServerInfo":{"DataCenter":"North Europe","Slice":"SliceA","Ring":"3","ScaleUnit":"003","Host":"AGSFE_IN_26","ADSiteName":"DUB"}}',
      'Duration',
      '112.2677',
      'Date',
      'Wed, 10 Jan 2018 16:51:17 GMT',
      'Connection',
      'close',
    ]
  );

nock('https://graph.microsoft.com:443', { encodedQueryParams: true })
  .get('/v1.0/me/mailFolders/Inbox/messages')
  .query({ '%24search': '%22body%3Agit%22' })
  .reply(
    200,
    {
      '@odata.context':
        "https://graph.microsoft.com/v1.0/$metadata#users('andrajevtic%40gmail.com')/mailFolders('Inbox')/messages",
      value: [
        {
          '@odata.etag': 'W/"CQAAABYAAAC4w7yzIi92T4cZYhykssKjAACe+DfR"',
          id:
            'AQMkADAwATM0MDAAMS1hOTJkLWY1ZmMtMDACLTAwCgBGAAADE_PgYjciLkG02frPzc8fxgcAuMO8syIvdk_HGWIcpLLCowAAAgEMAAAAuMO8syIvdk_HGWIcpLLCowAAAJ7mNrwAAAA=',
          createdDateTime: '2017-12-22T11:53:12Z',
          lastModifiedDateTime: '2017-12-22T11:53:12Z',
          changeKey: 'CQAAABYAAAC4w7yzIi92T4cZYhykssKjAACe+DfR',
          categories: [],
          receivedDateTime: '2017-12-22T11:53:12Z',
          sentDateTime: '2017-12-22T11:53:03Z',
          hasAttachments: true,
          internetMessageId: '<CAOx7vdabSUU0VcJ4-cGuiBQ=Jhf4eeAZmmtKGp81nZQSOYwJ=A@mail.gmail.com>',
          subject: 'Re: test',
          bodyPreview:
            'check out git cheat sheet\r\n\r\nOn Fri, Dec 22, 2017 at 12:49 PM, Andreja Jevtic <andrajevtic@gmail.com> wrote:\r\ntest',
          importance: 'normal',
          parentFolderId:
            'AQMkADAwATM0MDAAMS1hOTJkLWY1ZmMtMDACLTAwCgAuAAADE_PgYjciLkG02frPzc8fxgEAuMO8syIvdk_HGWIcpLLCowAAAgEMAAAA',
          conversationId: 'AQQkADAwATM0MDAAMS1hOTJkLWY1ZmMtMDACLTAwCgAQAGS9oV5Hh6pDkET2Bcv6BRI=',
          isDeliveryReceiptRequested: null,
          isReadReceiptRequested: false,
          isRead: false,
          isDraft: false,
          webLink:
            'https://outlook.live.com/owa/?ItemID=AQMkADAwATM0MDAAMS1hOTJkLWY1ZmMtMDACLTAwCgBGAAADE%2BPgYjciLkG02frPzc8fxgcAuMO8syIvdk%2BHGWIcpLLCowAAAgEMAAAAuMO8syIvdk%2BHGWIcpLLCowAAAJ7mNrwAAAA%3D&exvsurl=1&viewmodel=ReadMessageItem',
          inferenceClassification: 'focused',
          body: {
            contentType: 'html',
            content:
              '<html>\r\n<head>\r\n<meta http-equiv="Content-Type" content="text/html; charset=utf-8">\r\n<meta content="text/html; charset=utf-8">\r\n</head>\r\n<body>\r\n<div dir="ltr">check out git cheat sheet</div>\r\n<div class="gmail_extra"><br>\r\n<div class="gmail_quote">On Fri, Dec 22, 2017 at 12:49 PM, Andreja Jevtic <span dir="ltr">\r\n&lt;<a href="mailto:andrajevtic@gmail.com" target="_blank">andrajevtic@gmail.com</a>&gt;</span> wrote:<br>\r\n<blockquote class="gmail_quote" style="margin:0 0 0 .8ex; border-left:1px #ccc solid; padding-left:1ex">\r\n<div dir="ltr">test</div>\r\n</blockquote>\r\n</div>\r\n<br>\r\n</div>\r\n</body>\r\n</html>\r\n',
          },
          sender: { emailAddress: { name: 'Andreja Jevtic', address: 'andrajevtic@gmail.com' } },
          from: { emailAddress: { name: 'Andreja Jevtic', address: 'andrajevtic@gmail.com' } },
          toRecipients: [{ emailAddress: { name: 'ajevtic.test@outlook.com', address: 'ajevtic.test@outlook.com' } }],
          ccRecipients: [],
          bccRecipients: [],
          replyTo: [],
        },
      ],
    },
    [
      'Cache-Control',
      'private',
      'Transfer-Encoding',
      'chunked',
      'Content-Type',
      'application/json;odata.metadata=minimal;odata.streaming=true;IEEE754Compatible=false;charset=utf-8',
      'request-id',
      '9e411bcd-6fa6-4876-8b99-c06053efbc7e',
      'client-request-id',
      '9e411bcd-6fa6-4876-8b99-c06053efbc7e',
      'x-ms-ags-diagnostic',
      '{"ServerInfo":{"DataCenter":"North Europe","Slice":"SliceA","Ring":"3","ScaleUnit":"002","Host":"AGSFE_IN_7","ADSiteName":"DUB"}}',
      'OData-Version',
      '4.0',
      'Duration',
      '142.293',
      'Date',
      'Wed, 10 Jan 2018 16:51:18 GMT',
      'Connection',
      'close',
    ]
  );

nock('https://graph.microsoft.com:443', { encodedQueryParams: true })
  .get('/v1.0/me/mailFolders/Inbox/messages')
  .query({ '%24count': 'true' })
  .reply(
    200,
    {
      '@odata.context':
        "https://graph.microsoft.com/v1.0/$metadata#users('andrajevtic%40gmail.com')/mailFolders('Inbox')/messages",
      '@odata.count': 3,
      value: [
        {
          '@odata.etag': 'W/"CQAAABYAAAC4w7yzIi92T4cZYhykssKjAACe+DfR"',
          id:
            'AQMkADAwATM0MDAAMS1hOTJkLWY1ZmMtMDACLTAwCgBGAAADE_PgYjciLkG02frPzc8fxgcAuMO8syIvdk_HGWIcpLLCowAAAgEMAAAAuMO8syIvdk_HGWIcpLLCowAAAJ7mNrwAAAA=',
          createdDateTime: '2017-12-22T11:53:12Z',
          lastModifiedDateTime: '2017-12-22T11:53:12Z',
          changeKey: 'CQAAABYAAAC4w7yzIi92T4cZYhykssKjAACe+DfR',
          categories: [],
          receivedDateTime: '2017-12-22T11:53:12Z',
          sentDateTime: '2017-12-22T11:53:03Z',
          hasAttachments: true,
          internetMessageId: '<CAOx7vdabSUU0VcJ4-cGuiBQ=Jhf4eeAZmmtKGp81nZQSOYwJ=A@mail.gmail.com>',
          subject: 'Re: test',
          bodyPreview:
            'check out git cheat sheet\r\n\r\nOn Fri, Dec 22, 2017 at 12:49 PM, Andreja Jevtic <andrajevtic@gmail.com> wrote:\r\ntest',
          importance: 'normal',
          parentFolderId:
            'AQMkADAwATM0MDAAMS1hOTJkLWY1ZmMtMDACLTAwCgAuAAADE_PgYjciLkG02frPzc8fxgEAuMO8syIvdk_HGWIcpLLCowAAAgEMAAAA',
          conversationId: 'AQQkADAwATM0MDAAMS1hOTJkLWY1ZmMtMDACLTAwCgAQAGS9oV5Hh6pDkET2Bcv6BRI=',
          isDeliveryReceiptRequested: null,
          isReadReceiptRequested: false,
          isRead: false,
          isDraft: false,
          webLink:
            'https://outlook.live.com/owa/?ItemID=AQMkADAwATM0MDAAMS1hOTJkLWY1ZmMtMDACLTAwCgBGAAADE%2BPgYjciLkG02frPzc8fxgcAuMO8syIvdk%2BHGWIcpLLCowAAAgEMAAAAuMO8syIvdk%2BHGWIcpLLCowAAAJ7mNrwAAAA%3D&exvsurl=1&viewmodel=ReadMessageItem',
          inferenceClassification: 'focused',
          body: {
            contentType: 'html',
            content:
              '<html>\r\n<head>\r\n<meta http-equiv="Content-Type" content="text/html; charset=utf-8">\r\n<meta content="text/html; charset=utf-8">\r\n</head>\r\n<body>\r\n<div dir="ltr">check out git cheat sheet</div>\r\n<div class="gmail_extra"><br>\r\n<div class="gmail_quote">On Fri, Dec 22, 2017 at 12:49 PM, Andreja Jevtic <span dir="ltr">\r\n&lt;<a href="mailto:andrajevtic@gmail.com" target="_blank">andrajevtic@gmail.com</a>&gt;</span> wrote:<br>\r\n<blockquote class="gmail_quote" style="margin:0 0 0 .8ex; border-left:1px #ccc solid; padding-left:1ex">\r\n<div dir="ltr">test</div>\r\n</blockquote>\r\n</div>\r\n<br>\r\n</div>\r\n</body>\r\n</html>\r\n',
          },
          sender: { emailAddress: { name: 'Andreja Jevtic', address: 'andrajevtic@gmail.com' } },
          from: { emailAddress: { name: 'Andreja Jevtic', address: 'andrajevtic@gmail.com' } },
          toRecipients: [{ emailAddress: { name: 'ajevtic.test@outlook.com', address: 'ajevtic.test@outlook.com' } }],
          ccRecipients: [],
          bccRecipients: [],
          replyTo: [],
        },
        {
          '@odata.etag': 'W/"CQAAABYAAAC4w7yzIi92T4cZYhykssKjAACe+DfQ"',
          id:
            'AQMkADAwATM0MDAAMS1hOTJkLWY1ZmMtMDACLTAwCgBGAAADE_PgYjciLkG02frPzc8fxgcAuMO8syIvdk_HGWIcpLLCowAAAgEMAAAAuMO8syIvdk_HGWIcpLLCowAAAJ7mNrsAAAA=',
          createdDateTime: '2017-12-22T11:51:35Z',
          lastModifiedDateTime: '2017-12-22T11:51:36Z',
          changeKey: 'CQAAABYAAAC4w7yzIi92T4cZYhykssKjAACe+DfQ',
          categories: [],
          receivedDateTime: '2017-12-22T11:51:36Z',
          sentDateTime: '2017-12-22T11:51:33Z',
          hasAttachments: false,
          internetMessageId: '<CAOx7vdaAn6fUA2_HzAE3-fZ-1uMLDmPnsUkhYqdZ7-CE=xwSqQ@mail.gmail.com>',
          subject: 'This is some test subject',
          bodyPreview: 'And some fake content.',
          importance: 'normal',
          parentFolderId:
            'AQMkADAwATM0MDAAMS1hOTJkLWY1ZmMtMDACLTAwCgAuAAADE_PgYjciLkG02frPzc8fxgEAuMO8syIvdk_HGWIcpLLCowAAAgEMAAAA',
          conversationId: 'AQQkADAwATM0MDAAMS1hOTJkLWY1ZmMtMDACLTAwCgAQAJIpyphomvVDt-o3xAkdiHE=',
          isDeliveryReceiptRequested: null,
          isReadReceiptRequested: false,
          isRead: false,
          isDraft: false,
          webLink:
            'https://outlook.live.com/owa/?ItemID=AQMkADAwATM0MDAAMS1hOTJkLWY1ZmMtMDACLTAwCgBGAAADE%2BPgYjciLkG02frPzc8fxgcAuMO8syIvdk%2BHGWIcpLLCowAAAgEMAAAAuMO8syIvdk%2BHGWIcpLLCowAAAJ7mNrsAAAA%3D&exvsurl=1&viewmodel=ReadMessageItem',
          inferenceClassification: 'focused',
          body: {
            contentType: 'html',
            content:
              '<html>\r\n<head>\r\n<meta http-equiv="Content-Type" content="text/html; charset=utf-8">\r\n<meta content="text/html; charset=utf-8">\r\n</head>\r\n<body>\r\n<div dir="ltr">And some fake <b>content</b>.</div>\r\n</body>\r\n</html>\r\n',
          },
          sender: { emailAddress: { name: 'Andreja Jevtic', address: 'andrajevtic@gmail.com' } },
          from: { emailAddress: { name: 'Andreja Jevtic', address: 'andrajevtic@gmail.com' } },
          toRecipients: [{ emailAddress: { name: 'ajevtic.test@outlook.com', address: 'ajevtic.test@outlook.com' } }],
          ccRecipients: [],
          bccRecipients: [],
          replyTo: [],
        },
        {
          '@odata.etag': 'W/"CQAAABYAAAC4w7yzIi92T4cZYhykssKjAACe+DfL"',
          id:
            'AQMkADAwATM0MDAAMS1hOTJkLWY1ZmMtMDACLTAwCgBGAAADE_PgYjciLkG02frPzc8fxgcAuMO8syIvdk_HGWIcpLLCowAAAgEMAAAAuMO8syIvdk_HGWIcpLLCowAAAJ7mNrcAAAA=',
          createdDateTime: '2017-12-22T11:49:53Z',
          lastModifiedDateTime: '2017-12-22T11:49:53Z',
          changeKey: 'CQAAABYAAAC4w7yzIi92T4cZYhykssKjAACe+DfL',
          categories: [],
          receivedDateTime: '2017-12-22T11:49:53Z',
          sentDateTime: '2017-12-22T11:49:46Z',
          hasAttachments: false,
          internetMessageId: '<CAOx7vdauJjSUmKa-WddBkYm85UWcEfx4NGMxTPBhdrTAz6XQMw@mail.gmail.com>',
          subject: 'test',
          bodyPreview: 'test',
          importance: 'normal',
          parentFolderId:
            'AQMkADAwATM0MDAAMS1hOTJkLWY1ZmMtMDACLTAwCgAuAAADE_PgYjciLkG02frPzc8fxgEAuMO8syIvdk_HGWIcpLLCowAAAgEMAAAA',
          conversationId: 'AQQkADAwATM0MDAAMS1hOTJkLWY1ZmMtMDACLTAwCgAQAGS9oV5Hh6pDkET2Bcv6BRI=',
          isDeliveryReceiptRequested: null,
          isReadReceiptRequested: false,
          isRead: false,
          isDraft: false,
          webLink:
            'https://outlook.live.com/owa/?ItemID=AQMkADAwATM0MDAAMS1hOTJkLWY1ZmMtMDACLTAwCgBGAAADE%2BPgYjciLkG02frPzc8fxgcAuMO8syIvdk%2BHGWIcpLLCowAAAgEMAAAAuMO8syIvdk%2BHGWIcpLLCowAAAJ7mNrcAAAA%3D&exvsurl=1&viewmodel=ReadMessageItem',
          inferenceClassification: 'focused',
          body: {
            contentType: 'html',
            content:
              '<html>\r\n<head>\r\n<meta http-equiv="Content-Type" content="text/html; charset=utf-8">\r\n<meta content="text/html; charset=utf-8">\r\n</head>\r\n<body>\r\n<div dir="ltr">test</div>\r\n</body>\r\n</html>\r\n',
          },
          sender: { emailAddress: { name: 'Andreja Jevtic', address: 'andrajevtic@gmail.com' } },
          from: { emailAddress: { name: 'Andreja Jevtic', address: 'andrajevtic@gmail.com' } },
          toRecipients: [{ emailAddress: { name: 'ajevtic.test@outlook.com', address: 'ajevtic.test@outlook.com' } }],
          ccRecipients: [],
          bccRecipients: [],
          replyTo: [],
        },
      ],
    },
    [
      'Cache-Control',
      'private',
      'Transfer-Encoding',
      'chunked',
      'Content-Type',
      'application/json;odata.metadata=minimal;odata.streaming=true;IEEE754Compatible=false;charset=utf-8',
      'request-id',
      '8d06ea45-5e3f-4919-a986-65b6ba49c4ea',
      'client-request-id',
      '8d06ea45-5e3f-4919-a986-65b6ba49c4ea',
      'x-ms-ags-diagnostic',
      '{"ServerInfo":{"DataCenter":"North Europe","Slice":"SliceA","Ring":"2","ScaleUnit":"000","Host":"AGSFE_IN_5","ADSiteName":"DUB"}}',
      'OData-Version',
      '4.0',
      'Duration',
      '159.8409',
      'Date',
      'Wed, 10 Jan 2018 16:51:17 GMT',
      'Connection',
      'close',
    ]
  );

nock('https://graph.microsoft.com:443', { encodedQueryParams: true })
  .get('/wrong/v1.0/me/mailFolders/Inbox/messages')
  .reply(
    400,
    {
      error: {
        code: 'BadRequest',
        message: 'Invalid version',
        innerError: { 'request-id': 'a72d2e59-f36d-42a9-8678-8eece41aeebe', date: '2018-01-10T16:51:19' },
      },
    },
    [
      'Cache-Control',
      'private',
      'Transfer-Encoding',
      'chunked',
      'Content-Type',
      'application/json',
      'request-id',
      'a72d2e59-f36d-42a9-8678-8eece41aeebe',
      'client-request-id',
      'a72d2e59-f36d-42a9-8678-8eece41aeebe',
      'x-ms-ags-diagnostic',
      '{"ServerInfo":{"DataCenter":"North Europe","Slice":"SliceA","Ring":"3","ScaleUnit":"001","Host":"AGSFE_IN_25","ADSiteName":"DUB"}}',
      'Duration',
      '1907.7285',
      'Date',
      'Wed, 10 Jan 2018 16:51:19 GMT',
      'Connection',
      'close',
    ]
  );

/* eslint-enable */
