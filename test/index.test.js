// node core modules

// 3rd party modules
import _ from 'lodash';
import test from 'ava';

// internal modules
import { mock, record, persist } from './fixtures/http-mocking';
import initContext from './fixtures/utils';

const { unmocked } = process.env;

test.before(() => (unmocked ? record() : mock()));
test.after(() => unmocked && persist());

test.beforeEach(initContext);

/* Successful scenarios validations */

test.cb('validate messageFromFolder() with count query param', (t) => {
  const {
    serviceFactory, serviceOptions, stringProps, dateProps, validateEmailAddressFactory,
  } = t.context;

  const service = serviceFactory('https://graph.microsoft.com', serviceOptions);
  const query = {
    count: true,
  };
  service.messagesFromFolder(query, {}, (err, mailConfig) => {
    t.ifError(err);
    const { value: messages, '@odata.count': count } = mailConfig;
    const validateEmailAddress = validateEmailAddressFactory(t);

    t.true(_.isArray(messages));
    t.is(messages.length, 3);
    t.is(count, 3);

    messages.forEach((message) => {
      const {
        body, sender, from, toRecipients,
      } = message;

      stringProps.forEach(prop => t.true(_.isString(message[prop])));
      dateProps.forEach(prop => t.true(_.isFinite(Date.parse(message[prop]))));

      ['contentType', 'content'].forEach(prop => t.true(prop in body));
      validateEmailAddress(sender.emailAddress);
      validateEmailAddress(from.emailAddress);

      t.true(_.isArray(toRecipients));
      t.is(toRecipients.length, 1);
      toRecipients.forEach(recipient => validateEmailAddress(recipient.emailAddress));
      t.end();
    });
  });
});

test.cb('validate messageFromFolder() with search query', (t) => {
  const {
    serviceFactory, serviceOptions, stringProps, dateProps, validateEmailAddressFactory,
  } = t.context;

  const service = serviceFactory('https://graph.microsoft.com', serviceOptions);
  const query = {
    search: '"body:git"',
  };
  service.messagesFromFolder(query, {}, (err, mailConfig) => {
    t.ifError(err);
    const { value: messages } = mailConfig;
    const validateEmailAddress = validateEmailAddressFactory(t);

    t.true(_.isArray(messages));
    t.is(messages.length, 1);

    messages.forEach((message) => {
      const {
        body, sender, from, toRecipients,
      } = message;

      stringProps.forEach(prop => t.true(_.isString(message[prop])));
      dateProps.forEach(prop => t.true(_.isFinite(Date.parse(message[prop]))));

      ['contentType', 'content'].forEach(prop => t.true(prop in body));
      validateEmailAddress(sender.emailAddress);
      validateEmailAddress(from.emailAddress);

      t.true(_.isArray(toRecipients));
      t.is(toRecipients.length, 1);
      toRecipients.forEach(recipient => validateEmailAddress(recipient.emailAddress));
      t.end();
    });
  });
});

/* Error / Wrong input scenarios validations */

test.cb('error validation of messageFromFolder() with wrong id parameter', (t) => {
  const { serviceFactory, serviceOptions } = t.context;

  const query = {
    id: 'Wrong',
  };

  const service = serviceFactory('https://graph.microsoft.com', serviceOptions);

  service.messagesFromFolder(query, {}, (err) => {
    t.is(err.code, 'ErrorInvalidIdMalformed');
    t.end();
  });
});

test.cb('error validation while invoking service with wrong host name', (t) => {
  const { serviceFactory, serviceOptions } = t.context;

  const query = {
    id: 'Wrong',
  };

  const badHostName = 'wrong-host.com';
  const service = serviceFactory(`https://${badHostName}`, serviceOptions);

  service.messagesFromFolder(query, {}, (err) => {
    t.is(err.code, 'ENOTFOUND');
    t.is(err.hostname, badHostName);
    t.end();
  });
});

test.cb('error validation while invoking service with wrong url', (t) => {
  const { serviceFactory, serviceOptions } = t.context;

  const service = serviceFactory('https://graph.microsoft.com/wrong', serviceOptions);

  service.messagesFromFolder({}, {}, (err) => {
    t.is(err.code, 'BadRequest');
    t.is(err.message, 'Invalid version');
    t.end();
  });
});
