'use strict';

// node core modules

// 3rd party modules
const _ = require('lodash');

// internal modules

const omitDefaultRequestParams = (params, extraOmmit = []) =>
  _.omit(params, ['uri', 'url', 'method', 'qs', 'baseUrl', ...extraOmmit]);

/**
 * Method that picks valid query parameters and appends a $ to their keys, since microsoft
 * graph APIs expect $ sign in query params for v1.0
 * https://developer.microsoft.com/en-us/graph/docs/concepts/query_parameters
 *
 * @param {Object} query                Query object that holds information required by request uri
 * @param {Object} qsValidParameters    Valid query parameters
 */
const buildQueryParam = (query, qsValidParameters) =>
  _.mapKeys(_.pick(query, qsValidParameters), (value, key) => `$${key}`);

module.exports = {
  omitDefaultRequestParams,
  buildQueryParam,
};
