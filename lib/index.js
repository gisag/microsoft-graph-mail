'use strict';

// node core modules

// 3rd party modules
const _ = require('lodash');
const OniyiHttpClient = require('oniyi-http-client');
const credentialsPlugin = require('oniyi-http-plugin-credentials');

// internal modules
const methods = require('./methods');

/**
 * A Service factory that provides methods which retrieve data from microsoft graph.
 *
 * @param   {String} baseUrl             The base URL of the Microsoft Graph with which the service will be communicating.
 * @param   {Object} serviceOptions      An object of key-value pairs to set as defaults for the service.
 */
module.exports = (baseUrl, serviceOptions = {}) => {
  // params we'll use across the module
  const params = _.merge(
    {
      defaults: {
        authType: '',
        baseUrl: baseUrl.charAt(baseUrl.length - 1) === '/' ? baseUrl : `${baseUrl}/`,
      },
    },
    serviceOptions
  );

  // create a http client to be used throughout this service
  const httpClient = OniyiHttpClient(params);
  const { plugins = {} } = params;

  if (plugins.credentials) {
    httpClient.use(credentialsPlugin(plugins.credentials));
  }

  const service = {};

  Object.defineProperty(service, 'httpClient', { value: httpClient });
  Object.defineProperty(service, 'params', { value: params });

  _.assign(service, methods);

  return service;
};
