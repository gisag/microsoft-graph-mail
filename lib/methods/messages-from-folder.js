'use strict';

// node core modules

// 3rd party modules
const _ = require('lodash');
const Boom = require('boom');

// internal modules
const { omitDefaultRequestParams, buildQueryParam } = require('./utils');

const qsValidParameters = [
  'count',
  'expand',
  'filter',
  'format',
  'orderBy',
  'search',
  'select',
  'skip',
  'skipToken',
  'top',
];

/**
 * Retrieve user's messages from a specific folder.
 *
 * @param  {Object}   query               Query object that holds information required by request uri.
 * @param  {Object}   query.id            The mailFolder's unique identifier. You can use the following
 *                                        well-known names to access the corresponding folder:
 *                                        Inbox, Drafts, SentItems, DeletedItems.
 * @param  {Object}   options             Additional information used as default for every request options.
 * @param  {Function} callback            [description]
 */
function messagesFromFolder(query = {}, options, callback) {
  const { httpClient } = this;
  const { id = 'Inbox' } = query;

  // construct the request options
  const requestOptions = _.merge({}, omitDefaultRequestParams(options), {
    qs: buildQueryParam(query, qsValidParameters),
    headers: {
      accept: 'application/json',
    },
    json: true,
    uri: `v1.0/me/mailFolders/${id}/messages`,
  });

  httpClient.makeRequest(requestOptions, (requestError, response, body = {}) => {
    if (requestError) {
      callback(requestError);
      return;
    }

    const { error } = body;
    // expected
    // status codes: 200, 400, 401, 404
    // content-type: application/json
    if (!response || response.statusCode !== 200) {
      // https://www.npmjs.com/package/boom#new-boommessage-options
      const boomError = new Boom('received response with unexpected status code', { statusCode: response.statusCode });
      callback(error || boomError.output.payload);
      return;
    }

    callback(null, body);
  });
}

module.exports = messagesFromFolder;
