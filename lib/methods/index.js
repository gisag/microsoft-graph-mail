'use strict';

// node core modules

// 3rd party modules

// internal modules
const messagesFromFolder = require('./messages-from-folder');

module.exports = {
  messagesFromFolder,
};
